const authRoutes = [
  {
    path: '/auth',
    name: 'Auth',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      {
        path: '/login',
        name: 'Login',
        component: () => import('pages/Auth/Login.vue'),
        meta: {
          requiresGuest: true
        }
      }
    ]
  }
]

export default authRoutes
