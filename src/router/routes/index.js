// Routes
import authRoutes from './auth/index'
import dashboardRoutes from './dashboard/index'

const routes = [
  ...authRoutes,
  ...dashboardRoutes,

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    name: 'page404',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
