const dashboardRoutes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'Dashboard', component: () => import('pages/Index.vue') },
      {
        path: 'users',
        component: () => import('layouts/EmptyLayout.vue'),
        children: [
          { path: '', name: 'Users', component: () => import('pages/Users/Index.vue') },
          { path: 'create', name: 'CreateUser', component: () => import('pages/Users/Create.vue') },
          { path: ':id/edit', name: 'EditUser', component: () => import('pages/Users/Edit.vue') }
        ]
      },
      {
        path: 'transfer-requests',
        component: () => import('layouts/EmptyLayout.vue'),
        children: [
          { path: '', name: 'TransferRequests', component: () => import('pages/TransferRequests/Index.vue') },
          { path: ':id', name: 'TransferRequest', component: () => import('pages/TransferRequests/Show.vue') }
        ]
      },
      { path: 'update-password', name: 'UpdatePassword', component: () => import('pages/Profile/UpdatePassword.vue') }
    ]
  }
]

export default dashboardRoutes
