const build = to => {
  const USER_KEY = 'baem-user'

  const LOCAL_STORAGE_USER = localStorage.getItem(USER_KEY)

  const is404 = to.matched.some(record => (record.name === 'page404'))

  // Redirect unauthenticated users to login page when accessing dashboard route
  if (
    !LOCAL_STORAGE_USER
    && !to.matched.some(record => record.meta.requiresGuest)
    && !is404
    ) {
    return {
      name: 'Login',
      query: { redirect: to.fullPath }
    }
  }

  // Check if route requires to be logged out
  if (to.matched.some(record => record.meta.requiresGuest) && LOCAL_STORAGE_USER) {
    return {
      name: 'Dashboard'
    }
  }

  return null
}

export default { build }
