export const setRequests = (state, requests) => {
  state.requests = [...requests]
}

export const setRequest = (state, request) => {
  state.request = { ...request }
}
