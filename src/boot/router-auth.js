// Services
import nextRouteFactory from '../router/services/next-route-factory.service'

// Navigation guards before each request
const beforeEach = (to, from, next) => {
  const nextRoute = nextRouteFactory.build(to)

  nextRoute ? next(nextRoute) : next()
}

export default ({ router }) => {
  router.beforeEach((to, from, next) => beforeEach(to, from, next))
}
